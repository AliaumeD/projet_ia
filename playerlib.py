from random import randint
import movelib
import copy
import random
import numpy as np

class Joueur():

	def __init__(self, jeu, color):
		self.jeu = jeu
		self.color = color
	
	def donne_coup(self, jeu):
		pass
	

class Humain(Joueur):
	pass
	



class IA(Joueur):
	pass

class Passeur(IA):
    def donne_coup(self, jeu):
        return -1
    
class Random(IA):
    def donne_coup(self,game):
        arrayLen=len(game.goban.liste_coups_oks())-1
        ChiffreAleatoire=randint(0,arrayLen)
        return game.goban.liste_coups_oks()[ChiffreAleatoire]

def simulation(self, jeu):
    nb_coup=1
    while (not jeu.partie_finie):
        coups_valides = jeu.goban.liste_coups_oks()
        jeu.jouer(random.choice(coups_valides))
        nb_coup=nb_coup+1
    return (jeu.score(),nb_coup)

class MonteCarlo(IA):
    def donne_coup(self, jeu):
        nb_coup=0
        coups_ok = jeu.goban.liste_coups_oks()
        max_score = -np.infty
        max_coup = []
        max_nb_evals = 10
        for c in coups_ok:
            for i in range (max_nb_evals):
                copie = jeu.copy()
                copie.jouer(c)
                tuple=simulation(self,copie)
                nb_coup=nb_coup+tuple[1]
                score_coup_c = tuple[0]
            if score_coup_c > max_score:
                max_score = score_coup_c
                max_coup = c
        print("Nombre de coups: ",nb_coup)
        return max_coup

    
    